Feature: Edit Product

	Scenario: User melakukan edit produk dengan mengosongkan beberapa field
		When User klik button Akun
		Then User klik menu Daftar Jual Saya
		Given Delay 3 detik
		Then User klik salah satu produk
		Then User mengosongkan field nama produk
		Given Delay 3 detik
		Then User mendapatkan alert bahwa field nama produk tidak boleh kosong
		Then User mengosongkan field harga produk
		Given Delay 3 detik
		Then User mendapatkan alert bahwa field harga produk tidak boleh kosong
		Then User mengosongkan field lokasi produk
		Given Delay 3 detik
		Then User mendapatkan alert bahwa field lokasi produk tidak boleh kosong
		Given Delay 3 detik
		Then User back ke Akun saya
		Given Delay 3 detik
		
	Scenario: User melakukan edit nama produk dengan valid
		When User klik salah satu produk pada daftar jual
		Then User mengosongkan field nama produk
		Then User mengisi field nama produk dengan nama produk baru
		Then User klik button Perbarui Produk
		Given Delay 3 detik

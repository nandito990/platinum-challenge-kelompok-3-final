Feature: Search
	Sebagai User, saya ingin melakukan pencarian barang pada aplikasi Second Hand Store

	Scenario: User melakukan pencarian menggunakan keyword yang valid
		When User tap field Search
		Then User mengisi motor pada field search
		Given Delay 3 detik
		Then User melihat barang-barang yang memiliki keyword motor
		Then User klik button clear

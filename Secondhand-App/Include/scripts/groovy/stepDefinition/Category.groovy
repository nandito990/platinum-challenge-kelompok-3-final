package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Category {
	@When("User klik button Beranda")
	public void user_klik_button_Beranda() {
		Mobile.tap(findTestObject('Object Repository/Page_Category/btn_Beranda'), 0)
	}

	@Then("User klik kategori Elektronik")
	public void user_klik_kategori_Elektronik() {
		Mobile.tap(findTestObject('Object Repository/Page_Category/btn_category_elektronik'), 0)
	}

	@Given("Delay {double} detik")
	public void delay_detik(Double double1) {
		Mobile.delay(5.5, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User melihat barang-barang pada kategori Elektronik")
	public void user_melihat_barang_barang_pada_kategori_Elektronik() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Category/verify_produkelektronik'), 0)
	}

	@Then("User klik kategori Komputer dan Aksesoris")
	public void user_klik_kategori_Komputer_dan_Aksesoris() {
		Mobile.tap(findTestObject('Object Repository/Page_Category/btn_category_komdanaksesoris'), 0)
	}

	@Then("User melihat barang-barang pada kategori Komputer dan Aksesoris")
	public void user_melihat_barang_barang_pada_kategori_Komputer_dan_Aksesoris() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Category/verify_komdanaksesoris'), 0)
	}

	@Given("Di delay {int} detik")
	public void di_delay_detik(Integer int20) {
		Mobile.delay(20, FailureHandling.STOP_ON_FAILURE)
	}
}

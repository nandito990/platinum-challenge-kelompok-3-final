import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.clearText(findTestObject('Page_Registrasi/inputfield_NamaLengkap'), 0)

Mobile.clearText(findTestObject('Page_Registrasi/inputfield_Email'), 0)

Mobile.clearText(findTestObject('Page_Registrasi/inputfield_Password'), 0)

Mobile.clearText(findTestObject('Page_Registrasi/inputfield_NomorHP'), 0)

Mobile.clearText(findTestObject('Page_Registrasi/inputfield_Kota'), 0)

Mobile.clearText(findTestObject('Page_Registrasi/inputfield_Alamat'), 0)

Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_NamaLengkap'), 'Pukaxa a', 0)

Date email = new Date()

String emailDoctor = email.format('yyyyMMddHHmmss')

def email_code = ('aab' + emailDoctor) + '@getnada.com'

Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_Email'), email_code, 0)

Mobile.setEncryptedText(findTestObject('Object Repository/Page_Registrasi/inputfield_Password'), 'tzH6RvlfSTg=', 0)

Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_NomorHP'), '085445695220', 0)

Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_Kota'), 'Bandung', 0)

Mobile.setText(findTestObject('Object Repository/Page_Registrasi/inputfield_Alamat'), 'Jalan Kaki', 0)

Mobile.tap(findTestObject('Page_Registrasi/btn_daftar_Daftar'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Page_Registrasi/verify_passwordharus6karakter'), 0)


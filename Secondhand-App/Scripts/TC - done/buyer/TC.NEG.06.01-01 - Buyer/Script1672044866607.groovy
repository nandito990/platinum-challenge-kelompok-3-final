import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
Mobile.tap(findTestObject('Page_Profile/btn_beranda'), 0)

Mobile.tap(findTestObject('Negotiation/Buyer/Positive/kolom Cari di Second Chance'), 0)

Mobile.setText(findTestObject('Negotiation/Buyer/Positive/kolom ketik produk'), 'kabel', 0)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Negotiation/Buyer/Positive/btn kabel lan'), 0)

Mobile.tap(findTestObject('Negotiation/Buyer/Positive/nego kabel lan'), 0)

Mobile.setText(findTestObject('Negotiation/Buyer/Positive/nominal harga tawar'), '5000', 0)

Mobile.tap(findTestObject('Negotiation/Buyer/Positive/Button - Kirim'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Negotiation/Buyer/Positive/btn kabel usb'), 0)

Mobile.tap(findTestObject('Negotiation/Buyer/Positive/nego kabel usb'), 0)

Mobile.setText(findTestObject('Negotiation/Buyer/Positive/nominal harga tawar'), '17000', 0)

Mobile.tap(findTestObject('Negotiation/Buyer/Positive/Button - Kirim'), 0)

Mobile.pressBack()

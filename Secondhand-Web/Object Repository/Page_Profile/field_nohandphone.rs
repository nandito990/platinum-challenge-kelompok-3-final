<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field_nohandphone</name>
   <tag></tag>
   <elementGuidId>aaf7a24d-06b6-4140-ab56-8a9ae7031454</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@placeholder = '+62854263762']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>+62854263762</value>
      <webElementGuid>1699e21e-fdb8-4091-ac95-be8ab1ae0347</webElementGuid>
   </webElementProperties>
</WebElementEntity>

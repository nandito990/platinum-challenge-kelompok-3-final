$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/PlatinumChallenge_Kelompok3/Secondhand-Web/Include/features/EditProduk.feature");
formatter.feature({
  "name": "EditProduk",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User ingin edit produk yang di inginkan",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "klik tombol list",
  "keyword": "When "
});
formatter.match({
  "location": "EditProduk.klik_tombol_list()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Klik foto produk",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduk.klik_foto_produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Klik button edit produk",
  "keyword": "And "
});
formatter.match({
  "location": "EditProduk.klik_button_edit_produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengkosongkan nama produk",
  "keyword": "When "
});
formatter.match({
  "location": "EditProduk.user_mengkosongkan_nama_produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengkosongkan harga produk",
  "keyword": "When "
});
formatter.match({
  "location": "EditProduk.user_mengkosongkan_harga_produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengkosongkan Deskripsi",
  "keyword": "When "
});
formatter.match({
  "location": "EditProduk.user_mengkosongkan_Deskripsi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisikan valid edit",
  "keyword": "When "
});
formatter.match({
  "location": "EditProduk.user_mengisikan_valid_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik terbitkan",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduk.klik_terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik tombol list",
  "keyword": "When "
});
formatter.match({
  "location": "EditProduk.klik_tombol_list()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/PlatinumChallenge_Kelompok3/Secondhand-Web/Include/features/Login.feature");
formatter.feature({
  "name": "Login",
  "description": "\tUser ingin login pada website SecondHand",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User melakukan login akun tanpa mengisi semua input field yang dibutuhkan",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User klik button Masuk",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_klik_button_Masuk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik tombol masuk",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_klik_tombol_masuk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan login akun tanpa mengisi input field password",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User mengisi field email login dengan valid",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_mengisi_field_email_login_dengan_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik tombol masuk",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_klik_tombol_masuk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan login akun menggunakan email yang tidak terdaftar/salah",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Menghapus field password login",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.menghapus_field_password_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email login yang salah",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_mengisi_field_email_login_yang_salah()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password login \"123456789\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_mengisi_field_password_login(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik tombol masuk",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_klik_tombol_masuk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mendapatkan error message",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_mendapatkan_error_message()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan login akun menggunakan email yang belum diverifikasi/aktivasi",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Menghapus field email login",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.menghapus_field_email_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Menghapus field password login",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.menghapus_field_password_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email login yang belom di verifikasi",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_mengisi_field_email_login_yang_belom_di_verifikasi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password email yang belom di verif \"12345678\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_mengisi_field_password_email_yang_belom_di_verif(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik tombol masuk",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_klik_tombol_masuk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mendapatkan error message verif",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_mendapatkan_error_message_verif()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan login akun dengan menggunakan email dan password yang valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User menghapus field email login",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_menghapus_field_email_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User menghapus field password login",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_menghapus_field_password_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email login dengan valid",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_mengisi_field_email_login_dengan_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password login \"123456789\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_mengisi_field_password_login(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik tombol masuk",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_klik_tombol_masuk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/platinum-challenge-kelompok-3-final/Secondhand-App/Include/features/Login.feature");
formatter.feature({
  "name": "Login",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User melakukan login akun tanpa mengisi input field apapun",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User klik button Akun",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_klik_button_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Masuk",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_button_Masuk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Masuk Login",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_klik_button_Masuk_Login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mendapatkan alert bahwa field email login tidak boleh kosong",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_mendapatkan_alert_bahwa_field_email_login_tidak_boleh_kosong()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan login akun menggunakan email yang tidak terdaftar/salah",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User mengisi field email login dengan email yang tidak terdaftar",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_mengisi_field_email_login_dengan_email_yang_tidak_terdaftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password login dengan valid",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_mengisi_field_password_login_dengan_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Masuk Login",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_klik_button_Masuk_Login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan login akun menggunakan password yang tidak terdaftar/salah",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Mengosongkan field email yang tidak terdaftar",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.mengosongkan_field_email_yang_tidak_terdaftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mengosongkan field password yang valid",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.mengosongkan_field_password_yang_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email login dengan email yang valid",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_mengisi_field_email_login_dengan_email_yang_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password dengan password yang salah",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_mengisi_field_password_dengan_password_yang_salah()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Masuk Login",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_klik_button_Masuk_Login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan login akun menggunakan email dan password yang valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Mengosongkan field email login sebelumnya",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.mengosongkan_field_email_login_sebelumnya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mengosongkan field password login yang salah",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.mengosongkan_field_password_login_yang_salah()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email login dengan email yang valid",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_mengisi_field_email_login_dengan_email_yang_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password login dengan valid",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_mengisi_field_password_login_dengan_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Masuk Login",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_klik_button_Masuk_Login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User masuk ke halaman Akun Saya",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_masuk_ke_halaman_Akun_Saya()"
});
formatter.result({
  "status": "passed"
});
});
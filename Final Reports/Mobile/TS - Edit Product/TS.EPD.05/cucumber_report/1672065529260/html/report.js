$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/platinum-challenge-kelompok-3-final/Secondhand-App/Include/features/EditProduct.feature");
formatter.feature({
  "name": "Edit Product",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User melakukan edit produk dengan mengosongkan beberapa field",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User klik button Akun",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_klik_button_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik menu Daftar Jual Saya",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.user_klik_menu_Daftar_Jual_Saya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik salah satu produk",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.user_klik_salah_satu_produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengosongkan field nama produk",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.user_mengosongkan_field_nama_produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mendapatkan alert bahwa field nama produk tidak boleh kosong",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.user_mendapatkan_alert_bahwa_field_nama_produk_tidak_boleh_kosong()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengosongkan field harga produk",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.user_mengosongkan_field_harga_produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mendapatkan alert bahwa field harga produk tidak boleh kosong",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.user_mendapatkan_alert_bahwa_field_harga_produk_tidak_boleh_kosong()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengosongkan field lokasi produk",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.user_mengosongkan_field_lokasi_produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mendapatkan alert bahwa field lokasi produk tidak boleh kosong",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.user_mendapatkan_alert_bahwa_field_lokasi_produk_tidak_boleh_kosong()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User back ke Akun saya",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.user_back_ke_Akun_saya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan edit nama produk dengan valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User klik salah satu produk pada daftar jual",
  "keyword": "When "
});
formatter.match({
  "location": "EditProduct.user_klik_salah_satu_produk_pada_daftar_jual()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengosongkan field nama produk",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.user_mengosongkan_field_nama_produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field nama produk dengan nama produk baru",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.user_mengisi_field_nama_produk_dengan_nama_produk_baru()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Perbarui Produk",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.user_klik_button_Perbarui_Produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
});
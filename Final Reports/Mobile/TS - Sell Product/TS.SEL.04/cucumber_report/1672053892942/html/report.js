$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/platinum-challenge-kelompok-3-final/Secondhand-App/Include/features/SellProduct.feature");
formatter.feature({
  "name": "SellProduct",
  "description": "\tSebagai User, saya ingin menambah produk jualan saya pada aplikasi Second Hand Store",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User melakukan tambah produk dengan mengisi semua input field yang dibutuhkan dengan valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User klik button Akun",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_klik_button_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button +",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_klik_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field nama produk dengan \"Patung Motor\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_mengisi_field_nama_produk_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field harga produk dengan \"150000\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_mengisi_field_harga_produk_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik field Pilih Kategori",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_klik_field_Pilih_Kategori()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User memilih kategori Elektronik",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_memilih_kategori_Elektronik()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field lokasi",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_mengisi_field_lokasi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field deskripsi",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_mengisi_field_deskripsi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button upload foto produk",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_klik_button_upload_foto_produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik Galeri",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_klik_Galeri()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User memilih foto dari galeri",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_memilih_foto_dari_galeri()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Terbitkan",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_klik_button_Terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User melihat informasi produk berhasil diterbitkan",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_melihat_informasi_produk_berhasil_diterbitkan()"
});
formatter.result({
  "status": "passed"
});
});
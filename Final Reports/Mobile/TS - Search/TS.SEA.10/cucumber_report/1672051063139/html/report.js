$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/platinum-challenge-kelompok-3-final/Secondhand-App/Include/features/Search.feature");
formatter.feature({
  "name": "Search",
  "description": "\tSebagai User, saya ingin melakukan pencarian barang pada aplikasi Second Hand Store",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User melakukan pencarian menggunakan keyword yang valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User tap field Search",
  "keyword": "When "
});
formatter.match({
  "location": "Search.user_tap_field_Search()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi motor pada field search",
  "keyword": "Then "
});
formatter.match({
  "location": "Search.user_mengisi_motor_pada_field_search()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User melihat barang-barang yang memiliki keyword motor",
  "keyword": "Then "
});
formatter.match({
  "location": "Search.user_melihat_barang_barang_yang_memiliki_keyword_motor()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button clear",
  "keyword": "Then "
});
formatter.match({
  "location": "Search.user_klik_button_clear()"
});
formatter.result({
  "status": "passed"
});
});